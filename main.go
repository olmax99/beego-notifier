package main

import (
	_ "beego-notifier/routers"

	"github.com/beego/beego/v2/core/logs"

	beego "github.com/beego/beego/v2/server/web"
)

const (
	APP_VER = "0.1.1.0227"
)

var (
	log = logs.NewLogger(10000)
)

func main() {
	beego.BConfig.Log.AccessLogs = true
	beego.BConfig.WebConfig.Session.SessionOn = true

	// ---------- Logger------------
	logs.EnableFuncCallDepth(true)
	logs.SetLogger("console", "")

	log.Info("[main.go:25] APP_NAME: " + beego.BConfig.AppName + ", APP_VER: " + APP_VER)

	beego.Run()
}
