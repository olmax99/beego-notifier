// Copyright 2013 Beego Samples authors
//
// Licensed under the Apache License, Version 2.0 (the "License"): you may
// not use this file except in compliance with the License. You may obtain
// a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
// WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
// License for the specific language governing permissions and limitations
// under the License.

package controllers

import (
	"beego-notifier/models"
	"time"
)

// LongPollingController handles long polling requests.
type LongPollingController struct {
	BaseController
}

// Post method handles receive messages requests for LongPollingController.
func (this *LongPollingController) Post() {
	this.TplName = "index.tpl"
	sid := this.CruSession.SessionID(this.Ctx.Request.Context())

	content := this.GetString("content")
	longTask(sid, content)
}

// Fetch method handles fetch archives requests for LongPollingController.
func (this *LongPollingController) Fetch() {
	lastReceived, err := this.GetInt("lastReceived")
	if err != nil {
		return
	}

	sessID := this.CruSession.SessionID(this.Ctx.Request.Context())
	events := models.GetEvents(int(lastReceived), sessID)
	if len(events) > 0 {
		this.Data["json"] = events
		this.ServeJSON()
		return
	}

	// Wait for new message(s).
	ch := make(chan bool)
	waitingList.PushBack(ch)
	<-ch

	var j []models.TEvent
	j = models.TransformEvents(models.GetEvents(int(lastReceived), sessID))
	this.Data["json"] = &j
	this.ServeJSON()
}

func longTask(user, content string) {
	// TODO: This could be switched around to a silent success, and error as
	//  event_message or both with two queues
	if len(user) == 0 || len(content) == 0 {
		log.Error("missing session.")
	}

	// long Task
	time.Sleep(5 * time.Second)

	publish <- newEvent(models.EVENT_MESSAGE, user, content)
}
