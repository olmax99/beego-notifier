// Copyright 2013 Beego Samples authors
//
// Licensed under the Apache License, Version 2.0 (the "License"): you may
// not use this file except in compliance with the License. You may obtain
// a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
// WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
// License for the specific language governing permissions and limitations
// under the License.

package controllers

import (
	"container/list"
	"time"

	"beego-notifier/models"
)

type Subscription struct {
	Archive []models.Event      // All the events from the archive.
	New     <-chan models.Event // New events coming in.
}

func newEvent(ep models.EventType, user, msg string) models.Event {
	return models.Event{
		Type:      ep,
		User:      user,
		Timestamp: int(time.Now().Unix()),
		Content:   msg,
	}
}

var (
	// Channel for new another event type.
	// newevent = make(chan Newevent, 10)
	// Send events here to publish them.
	publish = make(chan models.Event, 10)
	// Long polling waiting list.
	waitingList = list.New()
	//newevent = list.New()
)

// This function handles all incoming chan messages.
func eventqueue() {
	for {
		select {
		// Add other types of events to publish
		// case new_event := <-newevent :
		// 	if Filter(newevent, new_event.Name) {
		// 		newevent.PushBack(new_event) // Add user to the end of list.
		// 		// Publish a JOIN event.
		// 		publish <- newEvent(models.EVENT_NEW, new_event.Name, "")
		// 		Info("Do something:", new_event.Name)
		// 	} else {
		// 		Info("Do something else:", new_event.Name)
		// 	}
		case event := <-publish:
			// Notify waiting list.
			for ch := waitingList.Back(); ch != nil; ch = ch.Prev() {
				ch.Value.(chan bool) <- true
				waitingList.Remove(ch)
			}

			models.NewArchive(event)

			if event.Type == models.EVENT_MESSAGE {
				log.Info("Message from " + event.User + "; Content: " + event.Content)
			}
		}
	}
}

func init() {
	go eventqueue()
}
