package controllers

import (
	"time"

	"github.com/beego/beego/v2/core/logs"
	beego "github.com/beego/beego/v2/server/web"
)

var (
	log = logs.NewLogger(10000)
)

type BaseController struct {
	beego.Controller // Embed struct that has stub implementation of the interface.
}

// Prepare will execute before the other methods
func (c *BaseController) Prepare() {
	// Provide Session by default

	sess := c.GetSession("notifiersessionID")
	if _, ok := sess.(map[string]interface{}); ok {
		c.Data["InSession"] = 1 // for login bar in header.tpl
	} else {
		m := make(map[string]interface{})
		m["timestamp"] = time.Now()
		c.SetSession("notifiersessionID", m)
	}
}

func (c *BaseController) Get() {
	c.TplName = "index.tpl"
}
