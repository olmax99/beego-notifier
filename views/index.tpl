{{template "base/base.html" .}}
{{define "head"}}
<title>beego-notifier</title>
{{end}}
{{define "body"}}
<div class="container">
    <h3>entry_box:</h3>
    <form class="form-inline">
        <div class="col-md-6 form-group">
            <input id="entrybox" type="text" class="form-control" onkeydown="if(event.keyCode==13)return false;" required>
        </div>
        <button id="entrybtn" type="button" class="btn btn-default">Enter</button>
    </form>
</div>

<div class="container">
    <h3>message_history</h3>
    <div id="message-box"></div>
    <!-- <div id=message-row class="row">
         <div id="message-box" class="column">
         <div  class="card" style="width: 18rem;">
         <div class="card-body">
         <h4 class="card-title">Card title</h5>
         <p id="message-content" class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
         <a href="#"><span class="close-message">close</span></a>
         </div>
         </div>
         </div>
         </div> -->

</div>

{{end}}
