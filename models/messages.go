// Copyright 2013 Beego Samples authors
//
// Licensed under the Apache License, Version 2.0 (the "License"): you may
// not use this file except in compliance with the License. You may obtain
// a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
// WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
// License for the specific language governing permissions and limitations
// under the License.

package models

import (
	"container/list"
	"strings"
	"time"

	"github.com/beego/beego/v2/core/logs"
)

type EventType int

const (
	// iota: integer constants autoincrement (keyword)
	EVENT_MESSAGE = iota
)

var (
	log = logs.NewLogger(10000)
)

type Event struct {
	Type      EventType // JOIN, MESSAGE
	User      string
	Timestamp int // Unix timestamp (secs)
	Content   string
}

// transformed event for visualizing
type TEvent struct {
	Type      EventType
	User      string
	Timestamp int    // required for 'lastReceived' logic
	Timeread  string // human readable
	Content   string
}

const archiveSize = 20

// Event archives.
var archive = list.New()

// NewArchive saves new event to archive list.
func NewArchive(event Event) {
	if archive.Len() >= archiveSize {
		archive.Remove(archive.Front())
	}
	archive.PushBack(event)
}

// GetEvents returns all events after lastReceived.
func GetEvents(lastReceived int, sessID string) []Event {
	events := make([]Event, 0, archive.Len())
	for event := archive.Front(); event != nil; event = event.Next() {
		e := event.Value.(Event)
		if e.Timestamp > int(lastReceived) && e.User == sessID {
			events = append(events, e)
		}
	}
	return events
}

// TransformEvent ...
func TransformEvents(obj []Event) []TEvent {
	tevents := make([]TEvent, 0, len(obj))
	for _, v := range obj {
		te := &TEvent{}
		if strings.Contains(v.User, "@") {
			te.User = v.User
		} else {
			te.User = v.User[0:6]
		}
		te.Type = v.Type
		te.Timestamp = v.Timestamp
		// NOTE: the Timeread transform is critical for longpolling.js
		// to blend out messages that have been showed once!!
		te.Timeread = time.Unix(int64(v.Timestamp), 0).String()
		te.Content = v.Content
		tevents = append(tevents, *te)
	}
	return tevents
}
