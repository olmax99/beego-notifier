var lastReceived = 0;
var isWait = false;

var fetch = function () {
  if (isWait) return;
  isWait = true;
  $.getJSON("/lp/fetch?lastReceived=" + lastReceived, function (data) {
    if (data == null) return;
    $.each(data, function (i, event) {
      var elem = document.createElement('div');

      switch (event.Type) {
        // case 0: // JOIN
        //     if (event.User == $('#uname').text()) {
        //         li.innerText = 'You joined the chat room.';
        //     } else {
        //         li.innerText = event.User + ' joined the chat room.';
        //     }
        //     break;
      case 0: // MESSAGE
        var time = document.createElement('span');
        var username = document.createElement('strong');
        var content = document.createElement('span');

        time.innerText = event.Timeread;
        username.innerText = event.User;
        content.innerText = event.Content;

        elem.appendChild(document.createTextNode('['));
        elem.appendChild(time);
        elem.appendChild(document.createTextNode(']'));
        elem.appendChild(username);
        elem.appendChild(document.createTextNode(': '));
        elem.appendChild(content);
        
        // Note: this only works due to 'lastReceived' and 'TransformEvents'
        // logic will set time as 'undefined' after first show
        if (!(time.innerText.indexOf('undefined') === 0)) {
          $('#message-box').append(elem);
        }

        break;
      }

      // each 'message column' is fading in and out at a certain interval
      $('#message-box').fadeIn(1000, function() {
        $('#message-box').delay(10000).fadeOut(1000)
      });

      lastReceived = event.Timestamp;
    });
    isWait = false;
  });
}

// Call fetch every 3 seconds
setInterval(fetch, 3000);

fetch();

$(document).ready(function () {

  var postConecnt = function () {
    var content = $('#entrybox').val();
    $.post("/lp/post", {
      content: content
    });
    $('#entrybox').val("");
  }

  $('#entrybtn').click(function () {
      postConecnt();
  });

  $(".close-message").click(function() {
    $("#message-box").stop(true).fadeOut("slow");
  });
});
